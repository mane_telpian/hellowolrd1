import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('This is Text'),
        ),
        body: Center(
          child: Text('''
          Hello World! \n
          I am Mane Telpian. \n
          I graduate May 2021. \n 
          "It's not a purse. It's a satchel. Besides, \n
          Indiana Jones has one" - Alan, The Hangover '''),
          ),
        ),
      );
  }
}
